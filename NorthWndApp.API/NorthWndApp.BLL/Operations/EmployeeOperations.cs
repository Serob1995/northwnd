﻿using Microsoft.Extensions.Logging;
using NorthWnd.Core.Abstractions;
using NorthWnd.Core.Abstractions.Operations;
using NorthWnd.Core.Business_Models.Employees_Models;
using NorthWnd.Core.Entities;
using NorthWnd.Core.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace NorthWnd.BLL.Operations
{
    public class EmployeeOperations : IEmployeeOperations
    {
        private readonly IRepositoryManager _repositories; 
        private readonly ILogger<EmployeeOperations> _logger;

        public EmployeeOperations(IRepositoryManager repositories, ILogger<EmployeeOperations> logger) 
        {
            _repositories = repositories;
            _logger = logger;
        }

        public IEnumerable<EmployeeViewModel> Get()
        {
            _logger.LogInformation("EmployeeOperations ------ Get method started");
            var data = _repositories.Employees.GetAll();

            var result = data.Select(x => new EmployeeViewModel
            {
                EmployeeId = x.EmployeeId,
                Address = x.Address,
                BirthDate = x.BirthDate,
                City = x.City,
                Country = x.Country,
                Extension = x.Extension,
                FirstName = x.FirstName,
                HireDate = x.HireDate,
                HomePhone = x.HomePhone,
                LastName = x.LastName,
                Notes = x.Notes,
                Photo = x.Photo,
                PhotoPath = x.PhotoPath,
                PostalCode = x.PostalCode,
                Region = x.Region,
                ReportsTo = x.ReportsTo,
                Title = x.Title,
                TitleOfCourtesy = x.TitleOfCourtesy
            });
            _logger.LogInformation("EmployeeOperations ------ Get method finished");
            return result;
        }

        public EmployeeViewModel GetById(int id)
        {
            _logger.LogInformation("EmployeeOperations ------ GetById method started");
            var employee = _repositories.Employees.Get(id) ?? throw new LogicException("Wrong Employee Id"); 
            var result = new EmployeeViewModel
            {
                EmployeeId = employee.EmployeeId,
                Address = employee.Address,
                BirthDate = employee.BirthDate,
                City = employee.City,
                Country = employee.Country,
                Extension = employee.Extension,
                FirstName = employee.FirstName,
                HireDate = employee.HireDate,
                HomePhone = employee.HomePhone,
                LastName = employee.LastName,
                Notes = employee.Notes,
                Photo = employee.Photo,
                PhotoPath = employee.PhotoPath,
                PostalCode = employee.PostalCode,
                Region = employee.Region,
                ReportsTo = employee.ReportsTo,
                Title = employee.Title,
                TitleOfCourtesy = employee.TitleOfCourtesy
            };
            _logger.LogInformation("EmployeeOperations ------ GetById method finished");
            return result;
        }
        public bool Create(AddEmployeeModel employee)
        {
            _logger.LogInformation("EmployeeOperations ------ Add method started");
            var employeeEntitiy = new Employee
            {
                Address = employee.Address,
                BirthDate = employee.BirthDate,
                City = employee.City,
                Country = employee.Country,
                Extension = employee.Extension,
                FirstName = employee.FirstName,
                HireDate = employee.HireDate,
                HomePhone = employee.HomePhone,
                LastName = employee.LastName,
                Notes = employee.Notes,
                Photo = employee.Photo,
                PhotoPath = employee.PhotoPath,
                PostalCode = employee.PostalCode,
                Region = employee.Region,
                ReportsTo = employee.ReportsTo,
                Title = employee.Title,
                TitleOfCourtesy = employee.TitleOfCourtesy
            };
            _repositories.Employees.Add(employeeEntitiy);
            _repositories.SaveChanges();
            _logger.LogInformation("EmployeeOperations ----- Add method finished");
            return true;
        }

        public bool Edit(EmployeeViewModel employee)
        {
            _logger.LogInformation("EmployeeOperations ----- Edit method started");
            var employeeToEdit = _repositories.Employees.Get(employee.EmployeeId) ?? throw new LogicException("Wrong Employee Id");
            employeeToEdit.Address = employee.Address;
            employeeToEdit.BirthDate = employee.BirthDate;
            employeeToEdit.City = employee.City;
            employeeToEdit.Country = employee.Country;
            employeeToEdit.EmployeeId = employee.EmployeeId;
            employeeToEdit.Extension = employee.Extension;
            employeeToEdit.FirstName = employee.FirstName;
            employeeToEdit.HireDate = employee.HireDate;
            employeeToEdit.HomePhone = employee.HomePhone;
            employeeToEdit.LastName = employee.LastName;
            employeeToEdit.Notes = employee.Notes;
            employeeToEdit.Photo = employee.Photo;
            employeeToEdit.PhotoPath = employee.PhotoPath;
            employeeToEdit.PostalCode = employee.PostalCode;
            employeeToEdit.Region = employee.Region;
            employeeToEdit.ReportsTo = employee.ReportsTo;
            employeeToEdit.Title = employee.Title;
            employeeToEdit.TitleOfCourtesy = employee.TitleOfCourtesy;
            _repositories.Employees.Update(employeeToEdit);
            _repositories.SaveChanges();
            _logger.LogInformation("EmployeeOperations ----- Edit method finished");
            return true;
        }
        public bool Delete(int id)
        {
            _logger.LogInformation("EmployeeOperations ------ Delete method started");
            
            var employee = _repositories.Employees.Get(id) ?? throw new LogicException("Wrong Employee Id"); 
            _repositories.Employees.Remove(employee);
            _repositories.SaveChanges();
            _logger.LogInformation("EmployeeOperations ------ Delete method finished");
            return true;
        }
    }
}
