﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using NorthWnd.Core.Abstractions;
using NorthWnd.Core.Abstractions.Operations;
using NorthWnd.Core.Business_Models;
using NorthWnd.Core.Entities;
using NorthWnd.Core.Enum;
using NorthWnd.Core.Exceptions;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace NorthWnd.BLL.Operations
{
    public class UserOperations : IUserOperations
    {
        private readonly IRepositoryManager _repositories;
        private readonly ILogger<UserOperations> _logger;
        public UserOperations(IRepositoryManager repositories, ILogger<UserOperations> logger)
        {
            _repositories = repositories;
            _logger = logger;
        }

        public async Task Login(LoginModel model, HttpContext context)
        {
            _logger.LogInformation("UserOperations ------ Login method started");
            User user = _repositories.Users.GetSingle(u => u.Email == model.Email && u.Password == model.Password)
                ?? throw new LogicException("Wrong username or password");
            _logger.LogInformation("UserOperations ------ Login method finished");
            await Authenticate(user, context); // Authentication         
        }

        public async Task Logout(HttpContext context)
        {
            await context.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public async Task Register(RegisterModel model, HttpContext context)
        {
            _logger.LogInformation("UserOperations ------ Register method started");
            User user = _repositories.Users.GetSingle(u => u.Email == model.Email);
            if (user == null)
            {
                // Add user in DB
                _repositories.Users.Add(new User
                {
                    Email = model.Email,
                    Password = model.Password,
                    Role = Role.User
                });
                await _repositories.SaveChangesAsync();
                _logger.LogInformation("UserOperations ------ Register method finished");
                await Authenticate(user, context); // Authentication
            }
            else
            {
                throw new LogicException("User already exists");
            }

        }


        private async Task Authenticate(User user, HttpContext context)
        {
            _logger.LogInformation("UserOperations ------ Autenticate method started");
            // create one claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim(ClaimsIdentity.DefaultRoleClaimType,user.Role.ToString())
            };
            // Create object ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            // Install Autentication cookies
            _logger.LogInformation("UserOperations ------ Autenticate method finished");
            await context.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }
    }
}
