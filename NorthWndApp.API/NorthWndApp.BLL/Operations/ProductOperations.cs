﻿using Microsoft.Extensions.Logging;
using NorthWnd.Core.Abstractions;
using NorthWnd.Core.Abstractions.Operations;
using NorthWnd.Core.Business_Models.NorthWNDQueryList_Models;
using NorthWnd.Core.Business_Models.Products_Models;
using NorthWnd.Core.Entities;
using NorthWnd.Core.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace NorthWnd.BLL.Operations
{
    public class ProductOperations : IProductOperations
    {
        private readonly IRepositoryManager _repositories;
        private readonly ILogger<ProductOperations> _logger;


        public ProductOperations(IRepositoryManager repositories, ILogger<ProductOperations> logger)
        {
            _repositories = repositories;
            _logger = logger;
        }

        public IEnumerable<ProductViewModel> Get()
        {
            _logger.LogInformation("ProductOperations ------ Get method started");
            var data = _repositories.Products.GetAll();

            var result = data.Select(x => new ProductViewModel
            {
                CategoryId = x.CategoryId,
                Discontinued = x.Discontinued,
                ProductId = x.ProductId,
                ProductName = x.ProductName,
                QuantityPerUnit = x.QuantityPerUnit,
                ReorderLevel = x.ReorderLevel,
                SupplierId = x.SupplierId,
                UnitPrice = x.UnitPrice,
                UnitsInStock = x.UnitsInStock,
                UnitsOnOrder = x.UnitsOnOrder
            });
            _logger.LogInformation("ProductOperations ------ Get method finished");
            return result;
        }
        public ProductViewModel GetById(int id)
        {
            _logger.LogInformation("ProductOperations ------ GetById method started");
            var product = _repositories.Products.Get(id) ?? throw new LogicException("Wrong Product Id");
            var result = new ProductViewModel
            {
                CategoryId = product.CategoryId,
                Discontinued = product.Discontinued,
                ProductId = product.ProductId,
                ProductName = product.ProductName,
                QuantityPerUnit = product.QuantityPerUnit,
                ReorderLevel = product.ReorderLevel,
                SupplierId = product.SupplierId,
                UnitPrice = product.UnitPrice,
                UnitsInStock = product.UnitsInStock,
                UnitsOnOrder = product.UnitsOnOrder

            };
            _logger.LogInformation("ProductOperations ------ GetById method finished");
            return result;
        }

        public bool Create(AddProductModel product)
        {
            _logger.LogInformation("ProductOperations ------ Create method started");
            var isSupplierExist = _repositories.Products.Any(x => x.SupplierId == product.SupplierId);
            if (!isSupplierExist)
            {
                return false;
            }
            var productEntity = new Product
            {
                CategoryId = product.CategoryId,
                Discontinued = product.Discontinued,
                ProductName = product.ProductName,
                QuantityPerUnit = product.QuantityPerUnit,
                ReorderLevel = product.ReorderLevel,
                SupplierId = product.SupplierId,
                UnitPrice = product.UnitPrice,
                UnitsInStock = product.UnitsInStock,
                UnitsOnOrder = product.UnitsOnOrder

            };
            _repositories.Products.Add(productEntity);
            _repositories.SaveChanges();
            _logger.LogInformation("ProductOperations ------ Create method finished");
            return true;
        }

        public bool Edit(ProductViewModel product)
        {
            _logger.LogInformation("ProductOperations ------ Edit method started");
            var productToEdit = _repositories.Products.Get(product.ProductId) ?? throw new LogicException("Wrong Product Id");
            productToEdit.CategoryId = product.CategoryId;
            productToEdit.Discontinued = product.Discontinued;
            productToEdit.ProductName = product.ProductName;
            productToEdit.QuantityPerUnit = product.QuantityPerUnit;
            productToEdit.ReorderLevel = product.ReorderLevel;
            productToEdit.SupplierId = product.SupplierId;
            productToEdit.UnitPrice = product.UnitPrice;
            productToEdit.UnitsInStock = product.UnitsInStock;
            productToEdit.UnitsOnOrder = product.UnitsOnOrder;
            _repositories.SaveChanges();
            _logger.LogInformation("ProductOperations ------ Edit method finished");
            return true;
        }

        public bool Delete(int id)
        {
            _logger.LogInformation("ProductOperations ------ Delete method started");
            var product = _repositories.Products.Get(id) ?? throw new LogicException("Wrong Product Id");
            _repositories.Products.Remove(product);
            _repositories.SaveChanges();
            _logger.LogInformation("ProductOperations ------ Delete method finished");
            return true;
        }

        public IEnumerable<ProductsNeedReordering> GetProductsNeedReorderings()
        {
            _logger.LogInformation("ProductOperation --- Query N22");
            return _repositories.Products.GetProductsNeedReorderings();
        }

        public IEnumerable<ProductsThatNeedReordering> GetProductsThatNeedReorderings()
        {
            _logger.LogInformation("ProductOperation --- Query N23");
            return _repositories.Products.GetProductsThatNeedReorderings();
        }

    }
}
