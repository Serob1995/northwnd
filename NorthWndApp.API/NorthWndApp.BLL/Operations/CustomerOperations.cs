﻿using Microsoft.Extensions.Logging;
using NorthWnd.Core.Abstractions;
using NorthWnd.Core.Abstractions.Operations;
using NorthWnd.Core.Business_Models.Customers_Models;
using NorthWnd.Core.Business_Models.NorthWNDQueryList_Models;
using NorthWnd.Core.Entities;
using NorthWnd.Core.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace NorthWnd.BLL.Operations
{
    public class CustomerOperations : ICustomerOperations
    {
        private readonly IRepositoryManager _repositories;
        private readonly ILogger<CustomerOperations> _logger;


        public CustomerOperations(IRepositoryManager repositories, ILogger<CustomerOperations> logger)
        {
            _repositories = repositories;
            _logger = logger;
        }

        public IEnumerable<CustomerViewModel> Get()
        {
            _logger.LogInformation("CustomerOperations ------ Get method started");
            var data = _repositories.Customers.GetAll();

            var result = data.Select(x => new CustomerViewModel
            {
                CustomerId = x.CustomerId,
                CompanyName = x.CompanyName,
                ContactName = x.ContactName,
                ContactTitle = x.ContactTitle,
                Address = x.Address,
                City = x.City,
                Country = x.Country,
                Fax = x.Fax,
                Phone = x.Phone,
                PostalCode = x.PostalCode,
                Region = x.Region
            });
            _logger.LogInformation("CustomerOperations ------ Get method finished");
            return result;
        }
        public CustomerViewModel GetById(string id)
        {
            _logger.LogInformation("CustomerOperations ------ GetById method started");
            var customer = _repositories.Customers.Get(id) ?? throw new LogicException("Wrong Customer Id");
            var result = new CustomerViewModel
            {
                CustomerId = customer.CustomerId,
                CompanyName = customer.CompanyName,
                ContactName = customer.ContactName,
                ContactTitle = customer.ContactTitle,
                Address = customer.Address,
                City = customer.City,
                Country = customer.Country,
                Fax = customer.Fax,
                Phone = customer.Phone,
                PostalCode = customer.PostalCode,
                Region = customer.Region
            };
            _logger.LogInformation("CustomerOperations------ GetById method finished");
            return result;
        }

        public bool Create(AddCustomerModel customer)
        {
            _logger.LogInformation("CustomerOperations------ Add method started");
            var customerEntity = new Customer
            {
                Address = customer.Address,
                City = customer.City,
                CompanyName = customer.CompanyName,
                ContactName = customer.ContactName,
                ContactTitle = customer.ContactTitle,
                Country = customer.Country,
                Fax = customer.Fax,
                Phone = customer.Phone,
                PostalCode = customer.PostalCode,
                Region = customer.Region
            };
            _repositories.Customers.Add(customerEntity);
            _repositories.SaveChanges();
            _logger.LogInformation("CustomerOperations------ Add method finished");
            return true;
        }

        public bool Edit(CustomerViewModel customer)
        {
            _logger.LogInformation("CustomerOperations------ Edit method started");
            var customerToEdit = _repositories.Customers.Get(customer.CustomerId) ?? throw new LogicException("Wrong Customer Id");
            customerToEdit.Address = customer.Address;
            customerToEdit.City = customer.City;
            customerToEdit.CompanyName = customer.CompanyName;
            customerToEdit.ContactName = customer.ContactName;
            customerToEdit.ContactTitle = customer.ContactTitle;
            customerToEdit.Country = customer.Country;
            customerToEdit.Fax = customer.Fax;
            customerToEdit.Phone = customer.Phone;
            customerToEdit.PostalCode = customer.PostalCode;
            customerToEdit.Region = customer.Region;
            _repositories.Customers.Update(customerToEdit);
            _repositories.SaveChanges();
            _logger.LogInformation("CustomerOperations ------ Edit method finished");
            return true;
        }


        public bool Delete(string id)
        {
            _logger.LogInformation("CustomerOperations ------ Delete method started");
            var customer = _repositories.Customers.Get(id) ?? throw new LogicException("Wrong Customer Id");
            _repositories.Customers.Remove(customer);
            _repositories.SaveChanges();
            _logger.LogInformation("CustomerOperations ------ Delete method finished");
            return true;
        }

        public IEnumerable<CustomersWithNoOrdersForEmployeeId4> Get4EmpId()
        {
            _logger.LogInformation("CustomersOperation --- Query N31");
            return _repositories.Customers.Get4EmpId();
        }

        public IEnumerable<CustomerListByRegion> GetCustomerListByRegions()
        {
            _logger.LogInformation("CustomerOperation --- Query N24");
            return _repositories.Customers.GetCustomerListByRegions();
        }

        public IEnumerable<CustomersWithOrders> GetCustomersWithNoOrders()
        {
            _logger.LogInformation("CustomersOperation --- Query N30");
            return _repositories.Customers.GetCustomersWithNoOrders();
        }

        public IEnumerable<HighValueCustomers> GetHighValueCustomers()
        {
            _logger.LogInformation("CustomersOperation --- Query N32");
            return _repositories.Customers.GetHighValueCustomers();
        }

        public IEnumerable<HighValueCustomersWithDiscount> GetHighValueCustomersWithDiscounts()
        {
            _logger.LogInformation("CustomersOperation --- Query N34");
            return _repositories.Customers.GetHighValueCustomersWithDiscounts();
        }

        public IEnumerable<TotalCustomers> GetTotalCustomers()
        {
            _logger.LogInformation("CustomerOperation --- Query N21");
            return _repositories.Customers.GetTotalCustomers();
        }

        public IEnumerable<HighValueCustomers> HighValueCustomersTotalOrders()
        {
            _logger.LogInformation("CustomersOperation --- Query N33");
            return _repositories.Customers.HighValueCustomersTotalOrders();
        }
    }
}
