﻿using Microsoft.Extensions.Logging;
using NorthWnd.Core.Abstractions;
using NorthWnd.Core.Abstractions.Operations;
using NorthWnd.Core.Business_Models.Suppliers_Models;
using NorthWnd.Core.Entities;
using NorthWnd.Core.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace NorthWnd.BLL.Operations
{
    public class SupplierOperations : ISupplierOperations
    {
        private readonly IRepositoryManager _repositories;
        private readonly ILogger<SupplierOperations> _logger;

        public SupplierOperations(IRepositoryManager repositories, ILogger<SupplierOperations> logger)
        {
            _repositories = repositories;
            _logger = logger;
        }

        public IEnumerable<SupplierViewModel> Get()
        {
            _logger.LogInformation("SupplierOperations ------ Get method started");
            var data = _repositories.Suppliers.GetAll();

            var result = data.Select(x => new SupplierViewModel
            {
                SupplierId = x.SupplierId,
                CompanyName = x.CompanyName,
                ContactName = x.ContactName,
                ContactTitle = x.ContactTitle,
                Address = x.Address,
                City = x.City,
                Country = x.Country,
                Fax = x.Fax,
                Phone = x.Phone,
                PostalCode = x.PostalCode,
                Region = x.Region,
                HomePage = x.HomePage
            });
            _logger.LogInformation("SupplierOperations ------ Get method finished");
            return result;
        }

        public SupplierViewModel GetById(int id)
        {
            _logger.LogInformation("SupplierOperations ------ GetById method started");
            var supplier = _repositories.Suppliers.Get(id) ?? throw new LogicException("Wrong Supplier Id");
            var result = new SupplierViewModel
            {
                SupplierId = supplier.SupplierId,
                CompanyName = supplier.CompanyName,
                ContactName = supplier.ContactName,
                ContactTitle = supplier.ContactTitle,
                Address = supplier.Address,
                City = supplier.City,
                Country = supplier.Country,
                Fax = supplier.Fax,
                Phone = supplier.Phone,
                PostalCode = supplier.PostalCode,
                Region = supplier.Region,
                HomePage = supplier.HomePage
            };
            _logger.LogInformation("SupplierOperations ------ GetById method finished");
            return result;
        }
        public bool Create(AddSupplierModel supplier)
        {
            _logger.LogInformation("SupplierOperations ------ Add method started");
            var supplierEntity = new Supplier
            {
                Address = supplier.Address,
                City = supplier.City,
                CompanyName = supplier.CompanyName,
                ContactName = supplier.ContactName,
                ContactTitle = supplier.ContactTitle,
                Country = supplier.Country,
                Fax = supplier.Fax,
                Phone = supplier.Phone,
                PostalCode = supplier.PostalCode,
                Region = supplier.Region
            };
            _repositories.Suppliers.Add(supplierEntity);
            _repositories.SaveChanges();
            _logger.LogInformation("SupplierOperations ------ Add method finished");
            return true;
        }
        public bool Edit(SupplierViewModel supplier)
        {
            _logger.LogInformation("SupplierOperations ------ Edit method started");
            var supplierToEdit = _repositories.Suppliers.Get(supplier.SupplierId) ?? throw new LogicException("Wrong Supplier Id");
            supplierToEdit.Address = supplier.Address;
            supplierToEdit.City = supplier.City;
            supplierToEdit.CompanyName = supplier.CompanyName;
            supplierToEdit.ContactName = supplier.ContactName;
            supplierToEdit.ContactTitle = supplier.ContactTitle;
            supplierToEdit.Country = supplier.Country;
            supplierToEdit.Fax = supplier.Fax;
            supplierToEdit.Phone = supplier.Phone;
            supplierToEdit.PostalCode = supplier.PostalCode;
            supplierToEdit.Region = supplier.Region;
            _repositories.Suppliers.Update(supplierToEdit);
            _repositories.SaveChanges();
            _logger.LogInformation("SupplierOperations ------ Edit method finished");
            return true;
        }
        public bool Delete(int id)
        {
            _logger.LogInformation("SupplierOperations ------ Delete method started");
            var supplier = _repositories.Suppliers.Get(id) ?? throw new LogicException("Wrong Supplier Id");
            _repositories.Suppliers.Remove(supplier);
            _repositories.SaveChanges();
            _logger.LogInformation("SupplierOperations ------ Delete method finished");
            return true;
        }

       
    }
}
