﻿using Microsoft.Extensions.Logging;
using NorthWnd.Core.Abstractions;
using NorthWnd.Core.Abstractions.Operations;
using NorthWnd.Core.Business_Models;
using NorthWnd.Core.Business_Models.NorthWNDQueryList_Models;
using NorthWnd.Core.Business_Models.OrdersModels;
using NorthWnd.Core.Entities;
using NorthWnd.Core.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace NorthWnd.BLL.Operations
{
    public class OrderOperations : IOrderOperations
    {
        private readonly IRepositoryManager _repositories;
        private readonly ILogger<OrderOperations> _logger;


        public OrderOperations(IRepositoryManager repositories, ILogger<OrderOperations> logger)
        {
            _repositories = repositories;
            _logger = logger;
        }

        public IEnumerable<OrderViewModel> Get()
        {
            _logger.LogInformation("OrderOperations ------ Get method started");
            var data = _repositories.Orders.GetAll();

            var result = data.Select(x => new OrderViewModel
            {
                OrderId = x.OrderId,
                ShipAddress = x.ShipAddress,
                ShipName = x.ShipName,
                Freight = x.Freight,
                OrderDate = x.OrderDate,
                RequiredDate = x.RequiredDate,
                ShipCountry = x.ShipCountry,
                ShippedDate = x.ShippedDate,
                ShipPostalCode = x.ShipPostalCode,
                ShipCity = x.ShipCity,
                ShipRegion = x.ShipRegion,
                ShipVia = x.ShipVia
            });
            _logger.LogInformation("OrderOperations------ Get method finished");
            return result;
        }
        public OrderViewModel GetById(int id)
        {
            _logger.LogInformation("OrderOperations------ GetById method started");
            var order = _repositories.Orders.Get(id) ?? throw new LogicException("Wrong Order Id");
            var result = new OrderViewModel
            {
               
                OrderId = order.OrderId,
                ShipAddress = order.ShipAddress,
                ShipName = order.ShipName,
                Freight = order.Freight,
                OrderDate = order.OrderDate,
                RequiredDate = order.RequiredDate,
                ShipCountry = order.ShipCountry,
                ShippedDate = order.ShippedDate,
                ShipPostalCode = order.ShipPostalCode,
                ShipCity = order.ShipCity,
                ShipRegion = order.ShipRegion,
                ShipVia = order.ShipVia
            };
            _logger.LogInformation("OrderOperations------ GetById method finished");
            return result;
        }

        public bool Create(AddOrderModel order)
        {
            _logger.LogInformation("OrderOperations------ Create method started");
            var isEmployeeExist = _repositories.Orders.Any(x => x.EmployeeId == order.EmployeeId);
            if (!isEmployeeExist)
            {
                return false;
            }
            var orderEntity = new Order
            {
                EmployeeId = order.EmployeeId,
                CustomerId = order.CustomerId,
                Freight = order.Freight,
                OrderDate = order.OrderDate,
                RequiredDate = order.RequiredDate,
                ShipAddress = order.ShipAddress,
                ShipCity = order.ShipCity,
                ShipCountry = order.ShipCountry,
                ShipName = order.ShipName,
                ShippedDate = order.ShippedDate,
                ShipPostalCode = order.ShipPostalCode,
                ShipRegion = order.ShipRegion,
                ShipVia = order.ShipVia
            };
            _repositories.Orders.Add(orderEntity);
            _repositories.SaveChanges();
            _logger.LogInformation("OrderOperations------ Create method finished");
            return true;
        }

        public bool Edit(OrderViewModel order)
        {
            _logger.LogInformation("OrderOperations------ Edit method started");
            var orderToEdit = _repositories.Orders.Get(order.OrderId) ?? throw new LogicException("Wrong Order Id"); 
            orderToEdit.OrderId = order.OrderId;
            orderToEdit.OrderDate = order.OrderDate;
            orderToEdit.ShipVia = order.ShipVia;
            orderToEdit.ShipRegion = order.ShipRegion;
            orderToEdit.ShipPostalCode = order.ShipPostalCode;
            orderToEdit.ShippedDate = order.ShippedDate;
            orderToEdit.ShipName = order.ShipName;
            orderToEdit.ShipCountry = order.ShipCountry;
            orderToEdit.ShipCity = order.ShipCity;
            orderToEdit.ShipAddress = order.ShipAddress;
            orderToEdit.RequiredDate = order.RequiredDate;
            orderToEdit.Freight = order.Freight;
            _repositories.Orders.Update(orderToEdit);
            _repositories.SaveChanges();
            _logger.LogInformation("OrderOperations------ Edit method finished");
            return true;
        }

        public bool Delete(int id)
        {
            _logger.LogInformation("OrderOperations------ Delete method started");
            var order = _repositories.Orders.Get(id) ?? throw new LogicException("Wrong Order Id");
            _repositories.Orders.Remove(order);
            _repositories.SaveChanges();
            _logger.LogInformation("OrderOperations------ Delete method finished");
            return true;
        }

        public IEnumerable<HighFreightOrders> GetHighFreightOrders()
        {
            _logger.LogInformation("OrderOperation --- Query N25");
            return _repositories.Orders.GetHighFreightOrders();
        }

        public IEnumerable<HighFreightOrders> GetHighFreightOrders1996()
        {
            _logger.LogInformation("OrderOperation --- Query N26");
            return _repositories.Orders.GetHighFreightOrders1996();
        }

        public IEnumerable<HighFreightOrders> GetHighFreight1996_1997()
        {
            _logger.LogInformation("OrderOperation --- Query N27");
            return _repositories.Orders.GetHighFreight1996_1997();
        }

        public IEnumerable<MonthEndOrders> GetMonthendOrders()
        {
            _logger.LogInformation("OrderOperation --- Query N35");
            return _repositories.Orders.GetMonthendOrders();
        }

        public IEnumerable<OrdersWithManyLineItems> GetOrdersWithManyLineItems()
        {
            _logger.LogInformation("OrderOperation --- Query N36");
            return _repositories.Orders.GetOrdersWithManyLineItems();
        }

        public IEnumerable<OrdersRandomAssortment> GetOrdersRandomAssortments()
        {
            _logger.LogInformation("OrderOperation --- Query N37");
            return _repositories.Orders.GetOrdersRandomAssortments();
        }

        public IEnumerable<OrdersAccidentalDoubleEntry> GetDoubleEntries()
        {
            _logger.LogInformation("OrderOperation --- Query N38");
            return _repositories.Orders.GetDoubleEntries();
        }

        public IEnumerable<OrdersAccidentalDoubleEntryDetails> GetDoubleEntriesDetails()
        {
            _logger.LogInformation("OrderOperation --- Query N39");
            return _repositories.Orders.GetDoubleEntriesDetails();
        }

        public IEnumerable<LateOrders> GetLateOrders()
        {
            _logger.LogInformation("OrderOperation --- Query N41");
            return _repositories.Orders.GetLateOrders();
        }

        public IEnumerable<LateOrdersWhichEmployees> GetWhichEmployees()
        {
            _logger.LogInformation("OrderOperation --- Query N42");
            return _repositories.Orders.WhichEmployees();
        }

        
    }
}
