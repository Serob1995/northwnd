﻿using System;

namespace NorthWnd.Core.Business_Models.NorthWNDQueryList_Models
{
    public class LateOrders
    {
        public int OrderId { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? RequiredDate { get; set; }
        public DateTime? ShippedDate { get; set; }
    }
}
