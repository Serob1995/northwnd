﻿namespace NorthWnd.Core.Business_Models.NorthWNDQueryList_Models
{
    public class ProductsNeedReordering
    {
        public int Productid { get; set; }
        public string ProductName { get; set; }
        public short? UnitsInStock { get; set; }
        public short? ReorderLevel { get; set; }
    }
}
