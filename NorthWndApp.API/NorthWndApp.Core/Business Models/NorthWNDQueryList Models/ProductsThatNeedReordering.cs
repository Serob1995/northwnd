﻿namespace NorthWnd.Core.Business_Models.NorthWNDQueryList_Models
{
    public class ProductsThatNeedReordering
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public short? UnitsInStock { get; set; }
        public short? UnitsOnOrder { get; set; }
        public short? ReorderLevel { get; set; }
        public bool Discontinued { get; set; }
    }
}
