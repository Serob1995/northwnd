﻿namespace NorthWnd.Core.Business_Models.NorthWNDQueryList_Models
{
    public class OrdersWithManyLineItems
    {
        public int OrderId { get; set; }
        public int TotalOrderDetails { get; set; }
    }
}
