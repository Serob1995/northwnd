﻿namespace NorthWnd.Core.Business_Models.NorthWNDQueryList_Models
{
    public class HighValueCustomers
    {
        public string CustomerId { get; set; }
        public string CompanyName { get; set; }
        public int OrderId { get; set; }
        public decimal OrderIdTotalOrderAmount { get; set; }
    }
}
