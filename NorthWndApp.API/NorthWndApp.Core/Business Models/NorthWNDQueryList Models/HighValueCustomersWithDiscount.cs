﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NorthWnd.Core.Business_Models.NorthWNDQueryList_Models
{
    public class HighValueCustomersWithDiscount
    {
        public string CustomerId { get; set; }
        public string CompanyName { get; set; }
        public decimal TotalsWithtDiscount { get; set; }
        public decimal TotalWithoutDiscount { get; set; }
    }
}
