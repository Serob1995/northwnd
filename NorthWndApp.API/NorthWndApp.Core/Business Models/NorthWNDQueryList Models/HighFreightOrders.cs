﻿namespace NorthWnd.Core.Business_Models.NorthWNDQueryList_Models
{
    public class HighFreightOrders
    {
        public string ShipCountry { get; set; }
        public decimal? AverageFreight { get; set; }
    }
}
