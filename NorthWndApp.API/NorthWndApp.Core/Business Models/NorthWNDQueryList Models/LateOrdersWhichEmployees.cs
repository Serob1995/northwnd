﻿namespace NorthWnd.Core.Business_Models.NorthWNDQueryList_Models
{
    public class LateOrdersWhichEmployees
    {
        public int? EmployeeId { get; set; }
        public string LastName { get; set; }
        public int TotalLateOrders { get; set; }
    }
}
