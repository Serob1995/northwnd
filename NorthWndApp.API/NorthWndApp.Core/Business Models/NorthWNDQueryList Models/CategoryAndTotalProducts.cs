﻿namespace NorthWnd.Core.Business_Models.NorthWNDQueryList_Models
{
    public class CategoryAndTotalProducts
    {
        public string CategoryName { get; set; }
        public int TotalProducts { get; set; }
    }
}
