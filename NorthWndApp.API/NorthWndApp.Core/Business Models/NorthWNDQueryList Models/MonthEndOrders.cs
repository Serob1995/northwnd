﻿using System;

namespace NorthWnd.Core.Business_Models.NorthWNDQueryList_Models
{
    public class MonthEndOrders
    {
        public int? EmployeeID { get; set; }
        public int? OrderId { get; set; }
        public DateTime? OrderDate { get; set; }
    }
}
