﻿namespace NorthWnd.Core.Business_Models.NorthWNDQueryList_Models
{
    public class CustomerListByRegion
    {
        public string CustomerId { get; set; }
        public string CompanyName { get; set; }
        public string Region { get; set; }
    }
}
