﻿using System;

namespace NorthWnd.Core.Exceptions
{
    public class LogicException: Exception
    {
        public LogicException(string message)
            :base(message)
        {

        }
    }
}
