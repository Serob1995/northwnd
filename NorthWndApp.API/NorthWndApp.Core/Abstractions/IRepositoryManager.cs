﻿using NorthWnd.Core.Abstractions.Repositories;
using System.Data;
using System.Threading.Tasks;

namespace NorthWnd.Core.Abstractions
{
    public interface IRepositoryManager
    {
        public IOrderRepository Orders { get; }
        public ISupplierRepository Suppliers { get; }
        public ICustomerRepository Customers { get; }
        public IEmployeeRepository Employees { get; }
        public IProductRepository Products { get; }
        public IUserRepository Users { get; }

        public IDatabaseTransaction BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted);
        void SaveChanges();
        Task SaveChangesAsync();
    }
}
