﻿using NorthWnd.Core.Entities;

namespace NorthWnd.Core.Abstractions.Repositories
{
    public interface IEmployeeRepository: IRepositoryBase<Employee>
    {

    }
}
