﻿using NorthWnd.Core.Entities;

namespace NorthWnd.Core.Abstractions.Repositories
{
    public interface IUserRepository: IRepositoryBase<User>
    {

    }
}
