﻿using System;
using System.Collections.Generic;

namespace NorthWnd.Core.Abstractions.Repositories
{
    public interface IRepositoryBase<T> where T : class
    {
        void Add(T entity);
        void AddRange(IEnumerable<T> entities);
        void Remove(T entity);
        void Update(T entity);
        IEnumerable<T> GetAll();
        T Get(int id);
        T Get(string id);
        bool Any(Func<T, bool> condition);
        T GetSingle(Func<T, bool> predicate);
    }
}
