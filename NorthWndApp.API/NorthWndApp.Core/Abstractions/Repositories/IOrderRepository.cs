﻿using NorthWnd.Core.Business_Models.NorthWNDQueryList_Models;
using NorthWnd.Core.Entities;
using System.Collections.Generic;

namespace NorthWnd.Core.Abstractions.Repositories
{
    public interface IOrderRepository: IRepositoryBase<Order>
    {
        IEnumerable<LateOrders> GetLateOrders();
        IEnumerable<HighFreightOrders> GetHighFreightOrders();
        IEnumerable<HighFreightOrders> GetHighFreightOrders1996();
        IEnumerable<HighFreightOrders> GetHighFreight1996_1997();
        IEnumerable<MonthEndOrders> GetMonthendOrders();
        IEnumerable<OrdersWithManyLineItems> GetOrdersWithManyLineItems();
        IEnumerable<OrdersRandomAssortment> GetOrdersRandomAssortments();
        IEnumerable<LateOrdersWhichEmployees> WhichEmployees();
        IEnumerable<OrdersAccidentalDoubleEntry> GetDoubleEntries();
        IEnumerable<OrdersAccidentalDoubleEntryDetails> GetDoubleEntriesDetails();
    }
}
