﻿using NorthWnd.Core.Business_Models.NorthWNDQueryList_Models;
using NorthWnd.Core.Entities;
using System.Collections.Generic;

namespace NorthWnd.Core.Abstractions.Repositories
{
    public interface IProductRepository: IRepositoryBase<Product>
    {
        IEnumerable<ProductsNeedReordering> GetProductsNeedReorderings();
        IEnumerable<ProductsThatNeedReordering> GetProductsThatNeedReorderings();
    }
}
