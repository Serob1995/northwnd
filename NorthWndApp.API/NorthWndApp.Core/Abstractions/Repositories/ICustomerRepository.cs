﻿using NorthWnd.Core.Business_Models.NorthWNDQueryList_Models;
using NorthWnd.Core.Entities;
using System.Collections.Generic;

namespace NorthWnd.Core.Abstractions.Repositories
{
    public interface ICustomerRepository: IRepositoryBase<Customer>
    {
        IEnumerable<CustomersWithOrders> GetCustomersWithNoOrders();
        IEnumerable<TotalCustomers> GetTotalCustomers();
        IEnumerable<CustomerListByRegion> GetCustomerListByRegions();
        IEnumerable<CustomersWithNoOrdersForEmployeeId4> Get4EmpId();
        IEnumerable<HighValueCustomers> GetHighValueCustomers();
        IEnumerable<HighValueCustomers> HighValueCustomersTotalOrders();
        IEnumerable<HighValueCustomersWithDiscount> GetHighValueCustomersWithDiscounts();
    }
}
