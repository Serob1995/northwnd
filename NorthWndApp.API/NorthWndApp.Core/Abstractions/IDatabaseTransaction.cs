﻿using System;

namespace NorthWnd.Core.Abstractions
{
    public interface IDatabaseTransaction: IDisposable
    {
        void Commit();
        void Rollback();
    }
}
