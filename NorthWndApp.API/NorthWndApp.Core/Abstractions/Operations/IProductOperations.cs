﻿using NorthWnd.Core.Business_Models.NorthWNDQueryList_Models;
using NorthWnd.Core.Business_Models.Products_Models;
using System.Collections.Generic;

namespace NorthWnd.Core.Abstractions.Operations
{
    public interface IProductOperations
    {
        IEnumerable<ProductViewModel> Get();
        ProductViewModel GetById(int id);
        bool Create(AddProductModel product);
        bool Edit(ProductViewModel product);
        bool Delete(int id);
        IEnumerable<ProductsNeedReordering> GetProductsNeedReorderings();
        IEnumerable<ProductsThatNeedReordering> GetProductsThatNeedReorderings();
    }
}
