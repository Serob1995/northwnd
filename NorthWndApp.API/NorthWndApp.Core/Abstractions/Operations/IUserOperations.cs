﻿using Microsoft.AspNetCore.Http;
using NorthWnd.Core.Business_Models;
using System.Threading.Tasks;

namespace NorthWnd.Core.Abstractions.Operations
{
    public interface IUserOperations
    {
        Task Logout(HttpContext context);
        Task Register(RegisterModel model, HttpContext context);
        Task Login(LoginModel model, HttpContext context);
    }
}
