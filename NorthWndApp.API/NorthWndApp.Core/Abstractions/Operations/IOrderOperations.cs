﻿using NorthWnd.Core.Business_Models;
using NorthWnd.Core.Business_Models.NorthWNDQueryList_Models;
using NorthWnd.Core.Business_Models.OrdersModels;
using System.Collections.Generic;

namespace NorthWnd.Core.Abstractions.Operations
{
    public interface IOrderOperations
    {
        IEnumerable<OrderViewModel> Get();
        OrderViewModel GetById(int id);
        bool Create(AddOrderModel order);
        bool Edit(OrderViewModel order);
        bool Delete(int id);
        IEnumerable<LateOrders> GetLateOrders();
        IEnumerable<LateOrdersWhichEmployees> GetWhichEmployees();
        IEnumerable<OrdersWithManyLineItems> GetOrdersWithManyLineItems();
        IEnumerable<OrdersRandomAssortment> GetOrdersRandomAssortments();
        IEnumerable<HighFreightOrders> GetHighFreightOrders();
        IEnumerable<HighFreightOrders> GetHighFreightOrders1996();
        IEnumerable<HighFreightOrders> GetHighFreight1996_1997();
        IEnumerable<MonthEndOrders> GetMonthendOrders();
        IEnumerable<OrdersAccidentalDoubleEntry> GetDoubleEntries();
        IEnumerable<OrdersAccidentalDoubleEntryDetails> GetDoubleEntriesDetails();
    }
}
