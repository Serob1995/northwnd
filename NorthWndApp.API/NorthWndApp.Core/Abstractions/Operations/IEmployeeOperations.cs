﻿using NorthWnd.Core.Business_Models.Employees_Models;
using System.Collections.Generic;

namespace NorthWnd.Core.Abstractions.Operations
{
    public interface IEmployeeOperations
    {
        IEnumerable<EmployeeViewModel> Get();
        EmployeeViewModel GetById(int id);
        bool Create(AddEmployeeModel employee);
        bool Edit(EmployeeViewModel employee);
        bool Delete(int id);
    }
}
