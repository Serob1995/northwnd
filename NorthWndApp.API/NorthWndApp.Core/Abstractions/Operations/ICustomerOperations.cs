﻿using NorthWnd.Core.Business_Models.Customers_Models;
using NorthWnd.Core.Business_Models.NorthWNDQueryList_Models;
using System.Collections.Generic;

namespace NorthWnd.Core.Abstractions.Operations
{
    public interface ICustomerOperations
    {
        IEnumerable<CustomerViewModel> Get();
        CustomerViewModel GetById(string id);
        bool Create(AddCustomerModel customer);
        bool Edit(CustomerViewModel customer);
        bool Delete(string id);
        IEnumerable<CustomersWithOrders> GetCustomersWithNoOrders();
        IEnumerable<TotalCustomers> GetTotalCustomers();
        IEnumerable<CustomerListByRegion> GetCustomerListByRegions();
        IEnumerable<CustomersWithNoOrdersForEmployeeId4> Get4EmpId();
        IEnumerable<HighValueCustomers> GetHighValueCustomers();
        IEnumerable<HighValueCustomers> HighValueCustomersTotalOrders();
        IEnumerable<HighValueCustomersWithDiscount> GetHighValueCustomersWithDiscounts();
    }
}
