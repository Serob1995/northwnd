﻿using NorthWnd.Core.Business_Models.Suppliers_Models;
using System.Collections.Generic;

namespace NorthWnd.Core.Abstractions.Operations
{
    public interface ISupplierOperations
    {
        IEnumerable<SupplierViewModel> Get();
        SupplierViewModel GetById(int id);
        bool Create(AddSupplierModel supplier);
        bool Edit(SupplierViewModel supplier);
        bool Delete(int id);
    }
}
