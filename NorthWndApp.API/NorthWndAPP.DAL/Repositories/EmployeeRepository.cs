﻿using NorthWnd.Core.Abstractions.Repositories;
using NorthWnd.Core.Entities;
using NorthWndAPP.DAL;

namespace NorthWnd.DAL.Repositories
{
    public class EmployeeRepository: RepositoryBase<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(NORTHWNDContext dbContext)
            : base (dbContext)
        {

        }
    }
}
