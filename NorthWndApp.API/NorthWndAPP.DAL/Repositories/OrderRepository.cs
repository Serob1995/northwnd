﻿using NorthWnd.Core.Abstractions.Repositories;
using NorthWnd.Core.Business_Models.NorthWNDQueryList_Models;
using NorthWnd.Core.Entities;
using System.Collections.Generic;
using System.Linq;

namespace NorthWndAPP.DAL.Repositories
{
    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        public OrderRepository(NORTHWNDContext dbContext)
            : base(dbContext)
        {
        }
        //27
        public IEnumerable<HighFreightOrders> GetHighFreight1996_1997()
        {
            var query = (from order in Context.Orders
                         where order.OrderDate.Value.Year > 1997
                         where order.OrderDate.Value.Year < 1998
                         group order by order.ShipCountry into g
                         select new HighFreightOrders
                         {
                             ShipCountry = g.Key,
                             AverageFreight = g.Average(x => x.Freight)
                         }).OrderByDescending(x => x.AverageFreight).Take(3);
            return query.ToList();
        }

        //25
        public IEnumerable<HighFreightOrders> GetHighFreightOrders()
        {
            var query = (from order in Context.Orders
                         group order by order.ShipCountry into g
                         select new HighFreightOrders
                         {
                             ShipCountry = g.Key,
                             AverageFreight = g.Average(x => x.Freight)
                         }).OrderByDescending(x => x.AverageFreight).Take(3);
            return query.ToList();
        }


        //26
        public IEnumerable<HighFreightOrders> GetHighFreightOrders1996()
        {
            var query = (from order in Context.Orders
                         where order.OrderDate.Value.Year == 1997
                         group order by order.ShipCountry into g
                         select new HighFreightOrders
                         {
                             ShipCountry = g.Key,
                             AverageFreight = g.Average(x => x.Freight)
                         }).OrderByDescending(x => x.AverageFreight).Take(3);
            return query.ToList();
        }


        //35
        public IEnumerable<MonthEndOrders> GetMonthendOrders()
        {
            var query = from order in Context.Orders
                        where order.OrderDate.HasValue &&
                        order.OrderDate.Value.AddDays(1).Month > order.OrderDate.Value.Month
                        orderby order.EmployeeId, order.OrderId
                        select new MonthEndOrders
                        {
                            OrderId = order.OrderId,
                            EmployeeID = order.EmployeeId,
                            OrderDate = order.OrderDate
                        };
            return query.ToList();
        }





        //36
        public IEnumerable<OrdersWithManyLineItems> GetOrdersWithManyLineItems()
        {
            var query = (from ord in Context.Orders
                         join oddet in Context.OrderDetails
                         on ord.OrderId equals oddet.OrderId
                         group ord by ord.OrderId into g
                         select new OrdersWithManyLineItems
                         {
                             OrderId = g.Key,
                             TotalOrderDetails = g.Count()
                         }).OrderByDescending(x => x.TotalOrderDetails).Take(10);
            return query.ToArray();
        }




        //37
        public IEnumerable<OrdersRandomAssortment> GetOrdersRandomAssortments()
        {
            var sum = (from od in Context.Orders
                       select od).Count();

            var query = (from ord in Context.Orders
                         select new OrdersRandomAssortment
                         {
                             OrderId = ord.OrderId
                         }).Take(2 * sum / 100).OrderBy(x => x.OrderId);
            return query.ToList();

        }


        //38
        public IEnumerable<OrdersAccidentalDoubleEntry> GetDoubleEntries()
        {
            var query = from orddet in Context.OrderDetails
                        where orddet.Quantity >= 60
                        group orddet by new { orddet.OrderId, orddet.Quantity } into g
                        where g.Count() > 1
                        select new OrdersAccidentalDoubleEntry
                        {
                            OrderId = g.Key.OrderId
                        };
            return query.ToList();
        }



        //39
        public IEnumerable<OrdersAccidentalDoubleEntryDetails> GetDoubleEntriesDetails()
        {
            var query = from order in Context.OrderDetails
                        where order.Quantity >= 60
                        group order by new { order.OrderId, order.Quantity } into g
                        where g.Count() > 1
                        select g.Key.OrderId;
            var res = from order in Context.OrderDetails
                      where query.Contains(order.OrderId)
                      select new OrdersAccidentalDoubleEntryDetails
                      {
                          OrderId = order.OrderId,
                          Discount = order.Discount,
                          ProductId = order.ProductId,
                          Quantity = order.Quantity,
                          UnitPrice = order.UnitPrice
                      };
            return res.ToList();
        }


        //41
        public IEnumerable<LateOrders> GetLateOrders()
        {

            var query = from order in Context.Orders
                        where order.ShippedDate > order.RequiredDate
                        select new LateOrders
                        {
                            OrderId = order.OrderId,
                            OrderDate = order.OrderDate,
                            RequiredDate = order.RequiredDate,
                            ShippedDate = order.ShippedDate
                        };

            return query.ToList();
        }



        //42
        public IEnumerable<LateOrdersWhichEmployees> WhichEmployees()
        {

            var query = (from order in Context.Orders
                         join emp in Context.Employees
                         on order.EmployeeId equals emp.EmployeeId
                         where order.ShippedDate >= order.RequiredDate
                         group new { order, emp }
                         by new { order.EmployeeId, emp.LastName } into g
                         select new LateOrdersWhichEmployees
                         {
                             EmployeeId = g.Key.EmployeeId,
                             LastName = g.Key.LastName,
                             TotalLateOrders = g.Count()

                         }).OrderByDescending(x => x.TotalLateOrders);

            return query.ToList();
        }
    }
}
