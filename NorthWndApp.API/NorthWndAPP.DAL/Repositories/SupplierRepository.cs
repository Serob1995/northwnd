﻿using NorthWnd.Core.Abstractions.Repositories;
using NorthWnd.Core.Entities;

namespace NorthWndAPP.DAL.Repositories
{
    public class SupplierRepository: RepositoryBase<Supplier>, ISupplierRepository
    {
        public SupplierRepository(NORTHWNDContext dbContext)
            : base(dbContext) 
        {
        } 
    }
}
