﻿using NorthWnd.Core.Abstractions.Repositories;
using NorthWnd.Core.Entities;
using NorthWndAPP.DAL;

namespace NorthWnd.DAL.Repositories
{
    public class UserRepository: RepositoryBase<User>, IUserRepository
    {
        public UserRepository(NORTHWNDContext dbContext)
           : base(dbContext)
        {
        }
    }
}
