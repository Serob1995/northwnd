﻿using NorthWnd.Core.Abstractions.Repositories;
using NorthWnd.Core.Business_Models.NorthWNDQueryList_Models;
using NorthWnd.Core.Entities;
using NorthWndAPP.DAL;
using System.Collections.Generic;
using System.Linq;

namespace NorthWnd.DAL.Repositories
{
    public class ProductRepository: RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(NORTHWNDContext dbContext)
           : base(dbContext)
        {
        }

        //22
        public IEnumerable<ProductsNeedReordering> GetProductsNeedReorderings()
        {
            var query = from product in Context.Products
                        where product.UnitsInStock < product.ReorderLevel
                        orderby product.ProductId
                        select new ProductsNeedReordering
                        {
                            Productid = product.ProductId,
                            ProductName = product.ProductName,
                            UnitsInStock = product.UnitsInStock,
                            ReorderLevel = product.ReorderLevel
                        };
            return query.ToList();

        }

        //23
        public IEnumerable<ProductsThatNeedReordering> GetProductsThatNeedReorderings()
        {
            var query = from product in Context.Products
                        where (product.UnitsInStock + product.UnitsOnOrder) <= product.ReorderLevel
                        where product.Discontinued == false
                        orderby product.ProductId
                        select new ProductsThatNeedReordering
                        {
                            ProductId = product.ProductId,
                            ProductName = product.ProductName,
                            UnitsInStock = product.UnitsInStock,
                            UnitsOnOrder = product.UnitsOnOrder,
                            ReorderLevel = product.ReorderLevel,
                            Discontinued = product.Discontinued
                        };
            return query.ToList();
        }
    }
}
