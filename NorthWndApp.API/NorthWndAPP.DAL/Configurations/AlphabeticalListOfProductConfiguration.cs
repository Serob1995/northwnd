﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NorthWnd.Core.Entities;

namespace NorthWnd.DAL.Configurations
{
    internal class AlphabeticalListOfProductConfiguration: IEntityTypeConfiguration<AlphabeticalListOfProduct>
    {
        public void Configure(EntityTypeBuilder<AlphabeticalListOfProduct> buider)
        {
            buider.HasNoKey();

            buider.ToView("Alphabetical list of products");

            buider.Property(e => e.CategoryId).HasColumnName("CategoryID");

            buider.Property(e => e.CategoryName)
                .IsRequired()
                .HasMaxLength(15);

            buider.Property(e => e.ProductId).HasColumnName("ProductID");

            buider.Property(e => e.ProductName)
                .IsRequired()
                .HasMaxLength(40);

            buider.Property(e => e.QuantityPerUnit).HasMaxLength(20);

            buider.Property(e => e.SupplierId).HasColumnName("SupplierID");

            buider.Property(e => e.UnitPrice).HasColumnType("money");
        }
    }
}
