using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NorthWnd.API.Middleware;
using NorthWnd.BLL.Operations;
using NorthWnd.Core.Abstractions;
using NorthWnd.Core.Abstractions.Operations;
using NorthWnd.Core.Entities;
using NorthWndAPP.DAL;

namespace NorthWnd.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {            
            services.AddDbContext<NORTHWNDContext>(x =>
            {
                x.UseSqlServer(Configuration.GetConnectionString("default"));
            });
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
              .AddCookie(options => //CookieAuthenticationOptions
              {
                  //   options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/Account/Login");
              });
            services.AddScoped<IOrderOperations, OrderOperations>();

            services.AddScoped<ISupplierOperations, SupplierOperations>();

            services.AddScoped<ICustomerOperations, CustomerOperations>();

            services.AddScoped<IEmployeeOperations, EmployeeOperations>();

            services.AddScoped<IProductOperations, ProductOperations>();

            services.AddScoped<IUserOperations, UserOperations>();

            services.AddScoped<IRepositoryManager, RepositoryManager>();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseMiddleware<ErrorHandlingMiddleware>();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
