﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NorthWnd.Core.Abstractions.Operations;
using NorthWnd.Core.Business_Models.Customers_Models;

namespace NorthWnd.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerOperations _customerOperation;

        public CustomersController(ICustomerOperations customerOperation)
        {
            _customerOperation = customerOperation;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var result = _customerOperation.Get();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(string id)
        {
            var result = _customerOperation.GetById(id);
            return Ok(result);
        }

        [Authorize(Roles = "Admin, User")]
        [HttpPost]
        public IActionResult Create([FromBody] AddCustomerModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            _customerOperation.Create(model);
            return Created("", model);
        }

        [Authorize(Roles = "Admin, User")]
        [HttpPut]
        public IActionResult EditCustomer([FromBody] CustomerViewModel model)
        {
            var result =  _customerOperation.Edit(model);
            return Ok(result);
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            _customerOperation.Delete(id);
            return Ok();
        }

        [HttpGet("total-customers")]
        public IActionResult TotalCustomers()
        {
            var result = _customerOperation.GetTotalCustomers();
            return Ok(result);
        }



        [HttpGet("customers-with-no-orders")]
        public IActionResult Customerswithnoorders()
        {
            var result = _customerOperation.GetCustomersWithNoOrders();
            return Ok(result);
        }

        [HttpGet("customer-list-by-regions")]
        public IActionResult Customerlistbyregions()
        {
            var result = _customerOperation.GetCustomerListByRegions();
            return Ok(result);
        }

        [HttpGet("employee-id-4")]
        public IActionResult CustomersNoOrdersEmployeeId4()
        {
            var result = _customerOperation.Get4EmpId();
            return Ok(result);
        }

        [HttpGet("high-value-customers")]
        public IActionResult HighValueCustomers()
        {
            var result = _customerOperation.GetHighValueCustomers();
            return Ok(result);
        }

        [HttpGet("high-value-customers-total-orders")]
        public IActionResult HighValueCustomersTotalOrders()
        {
            var result = _customerOperation.HighValueCustomersTotalOrders();
            return Ok(result);
        }


        [HttpGet("high-value-customers-with-discounts")]
        public IActionResult HighValueCustomersWithDiscounts()
        {
            var result = _customerOperation.GetHighValueCustomersWithDiscounts();
            return Ok(result);
        }
    }
}
