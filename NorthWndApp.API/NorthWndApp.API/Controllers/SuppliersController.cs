﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NorthWnd.Core.Abstractions.Operations;
using NorthWnd.Core.Business_Models.OrdersModels;
using NorthWnd.Core.Business_Models.Suppliers_Models;

namespace NorthWnd.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SuppliersController : ControllerBase
    {
        private readonly ISupplierOperations _supplierOperations;

        public SuppliersController(ISupplierOperations supplierOperations)
        {
            _supplierOperations = supplierOperations;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var result = _supplierOperations.Get();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var result = _supplierOperations.GetById(id);
            if (result == null)
                return BadRequest();
            return Ok(result);
        }

        [Authorize(Roles = "Admin, User")]
        [HttpPost]
        public IActionResult Add([FromBody] AddSupplierModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var check = _supplierOperations.Create(model);
            if (!check)
                return BadRequest();
            return Created("", model);
        }

        [Authorize (Roles ="Admin, User")]
        [HttpPut]
        public IActionResult Update([FromBody] SupplierViewModel model)
        {
            var valid = _supplierOperations.Edit(model);
            if (!valid)
                return BadRequest();
            return Ok();
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public IActionResult Remove(int id)
        {
            var order = _supplierOperations.Delete(id);
            if (!order)
                return BadRequest();
            return Ok();
        }
    }
}
