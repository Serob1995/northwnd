﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NorthWnd.Core.Abstractions.Operations;
using NorthWnd.Core.Business_Models.Products_Models;

namespace NorthWnd.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductOperations _productOperations;

        public ProductsController(IProductOperations productOperations)
        {
            _productOperations = productOperations;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var result = _productOperations.Get();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var result = _productOperations.GetById(id);
            return Ok(result);
        }

        [Authorize(Roles = "Admin, User")]
        [HttpPost]
        public IActionResult Add([FromBody] AddProductModel order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            _productOperations.Create(order);
            return Created("", order);
        }

        [Authorize(Roles = "Admin, User")]
        [HttpPut]
        public IActionResult Update([FromBody] ProductViewModel order)
        {
            _productOperations.Edit(order);
            return Ok();
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public IActionResult Remove(int id)
        {
            _productOperations.Delete(id);
            return Ok();
        }

        [HttpGet("products-need-reordering")]
        public IActionResult ProductsNeedReorderings()
        {
            var result = _productOperations.GetProductsNeedReorderings();
            return Ok(result);
        }

        [HttpGet("products-that-need-reordering")]
        public IActionResult ProductsThatNeedReorderings()
        {
            var result = _productOperations.GetProductsThatNeedReorderings();
            return Ok(result);
        }
    }
}
