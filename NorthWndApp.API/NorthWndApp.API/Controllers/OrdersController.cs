﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NorthWnd.Core.Abstractions.Operations;
using NorthWnd.Core.Business_Models;
using NorthWnd.Core.Business_Models.OrdersModels;

namespace NorthWnd.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderOperations _orderOperations;

        public OrdersController(IOrderOperations orderOperations)
        {
            _orderOperations = orderOperations;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var result = _orderOperations.Get();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var result = _orderOperations.GetById(id);
            return Ok(result);
        }

        [Authorize(Roles = "Admin, User")]
        [HttpPost]
        public IActionResult Add([FromBody] AddOrderModel order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();   
            }
            _orderOperations.Create(order);
            return Created("", order);
        }



        [Authorize(Roles = "Admin, User")]
        [HttpPut]
        public IActionResult Update([FromBody] OrderViewModel order)
        {
            var result = _orderOperations.Edit(order);
            return Ok(result);
        }



        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public IActionResult Remove(int id)
        {
            _orderOperations.Delete(id);
            return Ok();
        }
        


        [HttpGet("high-freight-orders")]
        public IActionResult GetHighFreight()
        {
            var result = _orderOperations.GetHighFreightOrders();
            return Ok(result);
        }



        [HttpGet("high-freight-orders-1996")]
        public IActionResult GetHighFreight1996()
        {
            var result = _orderOperations.GetHighFreightOrders1996();
            return Ok(result);
        }



        [HttpGet("high-freight-1996_1997")]
        public IActionResult HighFreight1996_1997()
        {
            var result = _orderOperations.GetHighFreight1996_1997();
            return Ok(result);
        }




        [HttpGet("monthend-orders")]
        public IActionResult GetMonthEndOrders()
        {
            var result = _orderOperations.GetMonthendOrders();
            return Ok(result);
        }



        [HttpGet("orders-with-many-line-items")]
        public IActionResult GetOrdersWithManyLine()
        {
            var result = _orderOperations.GetOrdersWithManyLineItems();
            return Ok(result);
        }



        [HttpGet("orders-random-assortments")]
        public IActionResult GetOrdersRandom()
        {
            var result = _orderOperations.GetOrdersRandomAssortments();
            return Ok(result);
        }



        [HttpGet("orders-double-entry")]
        public IActionResult OrdersDoubleEntry()
        {
            var result = _orderOperations.GetDoubleEntries();
            return Ok(result);
        }


        [HttpGet("orders-double-entry-details")]
        public IActionResult OrdersDoubleEntryDetails()
        {
            var result = _orderOperations.GetDoubleEntriesDetails();
            return Ok(result);
        }



        [HttpGet("late-orders")]
        public IActionResult GetLateOrders()
        {
            var result = _orderOperations.GetLateOrders();
            return Ok(result);
        }



        [HttpGet("which-employees")]
        public IActionResult GetLateOrdersWichEmployee()
        {
            var result = _orderOperations.GetWhichEmployees();
            return Ok(result);
        }
    }
}
