﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NorthWnd.Core.Abstractions.Operations;
using NorthWnd.Core.Business_Models.Employees_Models;

namespace NorthWnd.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeOperations _employeeOperations;
        public EmployeesController(IEmployeeOperations employeeOperation)
        {
            _employeeOperations = employeeOperation;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var result = _employeeOperations.Get();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var result = _employeeOperations.GetById(id);
            return Ok(result);
        }

        [Authorize(Roles = "Admin, User")]
        [HttpPost]
        public IActionResult AddEmployee([FromBody] AddEmployeeModel employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            _employeeOperations.Create(employee);
            return Created("", employee);
        }

        [Authorize(Roles = "Admin, User")]
        [HttpPut]
        public IActionResult Edit([FromBody] EmployeeViewModel employee)
        {
            _employeeOperations.Edit(employee);
            return Ok();
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public IActionResult Remove(int id)
        {
            _employeeOperations.Delete(id);
            return Ok();
        }
    }
}
